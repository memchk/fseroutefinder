﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Google.OrTools.ConstraintSolver;
using CsvHelper;
using System.Net;

namespace FSRouteFinder
{
    public class HubSpokeProblem
    {

        private int Depot => 0;
        private int Size => nodes.Count();
        private String hub;

        class IcaoDataRecord
        {
            public String icao { get; set; }
            public double lat { get; set; }
            public double lon { get; set; }
        }

        class JobRecord
        {
            public String FromIcao { get; set; }
            public String ToIcao { get; set; }
            public int Amount { get; set; }
            public decimal Pay { get; set; }
            public bool PtAssignment { get; set; }
        }

        class Node
        {   
            public String Name { get; set; }
            public double Lat { get; set; }
            public double Lon { get; set; }
            public int Demand { get; set; }
            public bool Pickup { get; set; }
            public int Pay { get; set; }
        }

        List<Node> nodes;

        public HubSpokeProblem(string _hub, string api_key)
        {

            hub = _hub.ToUpper();

            var icaodata = new CsvReader(File.OpenText(@"./icaodata.csv"));
            var icaodata_records = icaodata.GetRecords<IcaoDataRecord>().ToDictionary(x => x.icao, x => x);
            var webClient = new WebClient();
            var to_jobs = new CsvReader(new StringReader(webClient.DownloadString(String.Format(@"http://server.fseconomy.net/data?userkey={0}&format=csv&query=icao&search=jobsto&icaos={1}", api_key, hub))));
            var from_jobs = new CsvReader(new StringReader(webClient.DownloadString(String.Format(@"http://server.fseconomy.net/data?userkey={0}&format=csv&query=icao&search=jobsfrom&icaos={1}", api_key, hub))));

            this.nodes = new List<Node>();

            nodes.Add(new Node
            {
                Name = hub,
                Demand = 0,
                Pickup = true,
                Lat = icaodata_records[hub].lat,
                Lon = icaodata_records[hub].lon,
                Pay = 0
            });



            foreach (var x in to_jobs.GetRecords<JobRecord>())
            {
                if (x.PtAssignment == true)
                {
                    var not_hub = x.FromIcao.ToUpper();

                    nodes.Add(new Node
                    {
                        Name = not_hub,
                        Demand = x.Amount,
                        Pickup = true,
                        Pay = (int)Math.Round(x.Pay * 100),
                        Lat = icaodata_records[not_hub].lat,
                        Lon = icaodata_records[not_hub].lon,
                    });
                }
            }

            foreach (var x in from_jobs.GetRecords<JobRecord>())
            {
                if (x.PtAssignment == true)
                {
                    var not_hub = x.ToIcao.ToUpper();
                    nodes.Add(new Node
                    {
                        Name = not_hub,
                        Demand = x.Amount,
                        Pickup = false,
                        Pay = (int)Math.Round(x.Pay * 100),
                        Lat = icaodata_records[not_hub].lat,
                        Lon = icaodata_records[not_hub].lon,
                    });
                }
            }
        }

        public void Solve(int plane_capacity)
        {
            var model = new RoutingModel(nodes.Count, 1, Depot);
            NodeEvaluator2 distance_callback = new GreatCircleDistance(hub, nodes);
            NodeEvaluator2 pickup_callback = new DemandEvalulator(hub, nodes, true);
            NodeEvaluator2 delivery_callback = new DemandEvalulator(hub, nodes, false);
            model.AddDimension(pickup_callback, 0, plane_capacity, true, "pickup");
            model.AddDimension(delivery_callback, 0, plane_capacity, false, "delivery");
            model.SetArcCostEvaluatorOfAllVehicles(distance_callback);


            var pickup_dimension = model.GetDimensionOrDie("pickup");
            var delivery_dimension = model.GetDimensionOrDie("delivery");

            var solver = model.solver();

            for (int i = 0; i < nodes.Count; ++i)
            {
                var idx = model.NodeToIndex(i);
                solver.Add(pickup_dimension.CumulVar(idx) + delivery_dimension.CumulVar(idx) <= plane_capacity);
            }

            for (int order = 1; order < nodes.Count; ++order)
            {
                int[] orders = { order };
                model.AddDisjunction(orders, nodes[order].Pay);
            }


            RoutingSearchParameters search_parameters = RoutingModel.DefaultSearchParameters();
            search_parameters.FirstSolutionStrategy = FirstSolutionStrategy.Types.Value.BestInsertion;
            search_parameters.LocalSearchMetaheuristic = LocalSearchMetaheuristic.Types.Value.GuidedLocalSearch;
            search_parameters.TimeLimitMs = 60000;

            Console.WriteLine("Search");
            Assignment solution = model.SolveWithParameters(search_parameters);

            if (solution != null)
            {
                String output = "Total cost: " + solution.ObjectiveValue() + "\n";
                String route = "Route: ";
                long distance = 0;
                decimal pay = 0;
                
                long order = model.Start(0);
                if (model.IsEnd(solution.Value(model.NextVar(order))))
                {
                    route += "Empty";
                }
                else
                {
                    while (!model.IsEnd(order)) {
                        var local_index = model.IndexToNode(order);
                        IntVar local_pickup = pickup_dimension.CumulVar(local_index);
                        IntVar local_delivery = delivery_dimension.CumulVar(local_index);
                        route += nodes[local_index].Name + String.Format("({0})", solution.Value(local_pickup) + solution.Value(local_delivery)) + " -> ";
                        pay += (decimal)nodes[local_index].Pay / 100;
                        var previous_order = order;
                        order = solution.Value(model.NextVar(order));
                        distance += model.GetArcCostForVehicle(previous_order, order, 0);
                    }

                    var index = model.IndexToNode(order);
                    IntVar pickup = pickup_dimension.CumulVar(index);
                    IntVar delivery = delivery_dimension.CumulVar(index);
                    output += "Total Distance: " + distance / 415 + "\n";
                    route += nodes[index].Name + String.Format("({0})", solution.Value(pickup) + solution.Value(delivery)) + " -> ";
                    pay += (decimal)nodes[index].Pay / 100;
                }
                output += "Total pay: $" + pay + "\n";
                output += route + "\n";
                Console.WriteLine(output);
            } else
            {
                Console.WriteLine("No Solution!");
            }

            GC.KeepAlive(distance_callback);
            GC.KeepAlive(pickup_callback);
            GC.KeepAlive(delivery_callback);
        }


        class GreatCircleDistance : NodeEvaluator2
        {
            long[,] cost_matrix;
            public GreatCircleDistance(string hub, List<Node> nodes)
            {
                cost_matrix = new long[nodes.Count, nodes.Count];
                for (var i = 0; i < nodes.Count; i++)
                {
                    for (var j = 0; j < nodes.Count; j++)
                    {
                        if (nodes[i].Name == nodes[j].Name)
                        {
                            cost_matrix[i, j] = 0;
                        }
                        else
                        {
                            cost_matrix[i, j] = (long)Haversine.calculate(nodes[i].Lat, nodes[i].Lon, nodes[j].Lat, nodes[j].Lon) * 415;
                        }
                    }
                }
            }

            public override long Run(int from, int to)
            {

                return cost_matrix[from, to];
            }
        }

        class DemandEvalulator : NodeEvaluator2
        {
            long[] demand;
            public DemandEvalulator(string hub, List<Node> nodes, bool pickup)
            {
                demand = new long[nodes.Count];
                for (var i = 0; i < nodes.Count; i++)
                {
                    if(pickup != nodes[i].Pickup)
                    {
                        demand[i] = 0;
                    }
                    else if (pickup)
                    {
                        demand[i] = nodes[i].Demand;
                    }
                    else
                    {
                        demand[i] = -nodes[i].Demand;
                    }
                }
            }

            public override long Run(int from, int to)
            {
                if (from < demand.Length)
                {
                    return demand[from];
                }
                else
                {
                    return 0;
                }
            }
        }
    }
}
